package br.com.itau.simulacaodeinvestimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimulacaoDeInvestimentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimulacaoDeInvestimentoApplication.class, args);
	}

}
