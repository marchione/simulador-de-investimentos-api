package br.com.itau.simulacaodeinvestimento.web.dto;

import br.com.itau.simulacaodeinvestimento.persistence.Investimento;

public class InvestimentoPayload {
    private double valor;
    private int quantidadeDeMeses;
    private double montante;

    public InvestimentoPayload(){}

    public InvestimentoPayload(Investimento investimento){
        setValor(investimento.getValor());
        setQuantidadeDeMeses(investimento.getQuantidadeDeMeses());
        setMontante(investimento.getMontante());
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getQuantidadeDeMeses() {
        return quantidadeDeMeses;
    }

    public void setQuantidadeDeMeses(int quantidadeDeMeses) {
        this.quantidadeDeMeses = quantidadeDeMeses;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public Investimento criarEntidade(){
        Investimento investimento = new Investimento();
        investimento.setValor(valor);
        investimento.setQuantidadeDeMeses(quantidadeDeMeses);

        return investimento;
    }
}
