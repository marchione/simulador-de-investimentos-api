package br.com.itau.simulacaodeinvestimento.web;

import br.com.itau.simulacaodeinvestimento.persistence.Investimento;
import br.com.itau.simulacaodeinvestimento.service.InvestimentoService;
import br.com.itau.simulacaodeinvestimento.web.dto.InvestimentoPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class SimulacaoController {

    @Autowired
    InvestimentoService investimentoService;

    @PostMapping("/investimento")
    public InvestimentoPayload create(@RequestBody InvestimentoPayload investimentoPayload){
        Investimento investimento = investimentoPayload.criarEntidade();

        return new InvestimentoPayload(investimentoService.criar(investimento));
    }

    @GetMapping("/investimentos")
    public List<InvestimentoPayload> recuperarTodasOsInvestimentos(){
        List<InvestimentoPayload> investimentosPayload = new ArrayList<>();

        for(Investimento investimento : investimentoService.recuperarTodos()){
            investimentosPayload.add(new InvestimentoPayload(investimento));
        }

        return investimentosPayload;
    }
}
