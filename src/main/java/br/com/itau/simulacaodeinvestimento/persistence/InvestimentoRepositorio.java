package br.com.itau.simulacaodeinvestimento.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InvestimentoRepositorio extends CrudRepository<Investimento, Integer> {
    List<Investimento> findAll();
}

