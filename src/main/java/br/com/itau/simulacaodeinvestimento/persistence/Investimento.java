package br.com.itau.simulacaodeinvestimento.persistence;

import javax.persistence.*;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private double valor;
    private int quantidadeDeMeses;
    private double montante;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getQuantidadeDeMeses() {
        return quantidadeDeMeses;
    }

    public void setQuantidadeDeMeses(int quantidadeDeMeses) {
        this.quantidadeDeMeses = quantidadeDeMeses;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}
