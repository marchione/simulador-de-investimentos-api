package br.com.itau.simulacaodeinvestimento.service;

import br.com.itau.simulacaodeinvestimento.persistence.Investimento;
import br.com.itau.simulacaodeinvestimento.persistence.InvestimentoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepositorio investimentoRepositorio;

    public Investimento criar(Investimento investimento){

        double montante = calcularMontante(investimento.getQuantidadeDeMeses(), investimento.getValor());
        investimento.setMontante(montante);

        investimentoRepositorio.save(investimento);

        return investimento;
    }

    private double calcularMontante(int quantidadeDeMeses, double valor){
        return valor*(Math.pow(1.007, quantidadeDeMeses));
    }

    public List<Investimento> recuperarTodos() {
        return investimentoRepositorio.findAll();
    }
}
